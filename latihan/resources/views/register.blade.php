<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Form Sign Up</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="post">
		@csrf
		<label for="firstname">First Name:</label><br><br>
		<input type="text" id="firstname" name="firstname"><br><br>
		<label for="lastname">Last Name:</label><br><br>
		<input type="text" id="lastname" name="lastname"><br><br>
		<p>Gender:</p>
		<input type="radio" id="male" name="gender"><label for="male">Male</label>
		<br>
		<input type="radio" id="female" name="gender"><label for="female">Female</label>
		<br>
		<input type="radio" id="other" name="gender"><label for="other">Other</label>
		<br><br>
		<p>Nationality</p>
		<select name="Nationality">
			<option>Brunei</option><br>
			<option>Cambodia</option><br>
			<option>Indonesia</option><br>
			<option>Laos</option><br>
			<option>Malaysia</option><br>
			<option>Myanmar</option><br>
			<option>Philipines</option>
			<option>Singapore</option><br>
			<option>Thailand</option><br>
			<option>Timor Leste</option>
			<option>Vietnam</option>
		</select><br><br>
		<p>Language Spoken:</p>
		<input type="checkbox" id="bahasa" name="lang"><label for="bahasa">Bahasa Indonesia</label><br>
		<input type="checkbox" id="eng" name="lang"><label for="eng">English</label><br>
		<input type="checkbox" id="lain" name="lang"><label for="lain">Other</label><br>
		<br><br>
		<label for="bio">Bio:</label><br><br>
		<textarea id="bio"></textarea><br>
		<input type="submit" value="Sign Up">
		<!--<button type="submit"><a href="welcome.html" style="text-decoration: none;">Sign Up</a></button>-->
	</form>
</body>
</html>